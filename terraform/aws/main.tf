resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQxcV/SuM+8Z1pC2l6z3WRLPldTORjkiYA4j0TvzHFhXu92roYXJu/szxGUFM4RnPl4GPFYdtC319rKdNrqdpeDDt50hBmk69eWxfqIIM1rPs7MCfsjAIEYnjM15Q/sX48l1zb8a6Y7Qa4/H3fnroTiRziiA+4lUQfhkUrYFn+G3UFb0Q6QImLmXbzFfCcmnzh2ahE+LxtRjXPwnq8U9JsiGJ+N3h1AGTbIEUZRal8GyL5bhl0yFPrI0NEN/ftOoEGfE4ZCt+dA2qTvjTDmT1D+HIRS9f/7Cl+phT/T+uY6N2KMGecPJDzxcsYF37fEyFfUHO/uB+ruVwXBcExEMmduNHvtd/WHpBSosSJPIcXle8Wx8AotrSg3asYw96EjNxQ5AxcJamzNUGZH7gDWAr6Ot0NUNF0e7Z8WGmTFqXj97ggMRttu2wRHsyWhA8IhTr5SyXpDqeyfQ1b+vWcvK8tIwXyAZUY3VqMznnEtZTX1f27yC7cSgwNXksEp3EDCnM= alexis@Alexis"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}

